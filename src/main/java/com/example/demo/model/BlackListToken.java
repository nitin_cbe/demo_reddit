package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;


@Data
@AllArgsConstructor
@NoArgsConstructor

@Document(collection = "blackListToken" )
public class BlackListToken{

    @Id
    private String blckListId;
    private String blackToken;
    @Field
    @Indexed(name="registeredDate", expireAfterSeconds=86400)
    private LocalDateTime registeredDate;

}