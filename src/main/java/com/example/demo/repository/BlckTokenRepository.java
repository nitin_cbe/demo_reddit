package com.example.demo.repository;

import com.example.demo.model.BlackListToken;
import com.example.demo.model.RefreshToken;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface BlckTokenRepository extends MongoRepository<BlackListToken, Long> {

    Optional<BlackListToken> findByBlackToken(String token);
}
