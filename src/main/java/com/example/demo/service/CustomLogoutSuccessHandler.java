package com.example.demo.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

@Component
public class CustomLogoutSuccessHandler implements  LogoutSuccessHandler {

    @Autowired
    private RefreshTokenService refreshTokenService;


//    @PostMapping("/logout")
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.setHeader("Access-Control-Allow-Origin", "*");

//        log.info("TodosLogoutHandler logging you out of the back-end app.")

        String refreshtoken = request.getParameter("refreshtoken");
        String authenticationToken = request.getParameter("authenticationToken");
        System.out.println(refreshtoken+"=======rrrrrrrrrrrrrr");
        System.out.println(authenticationToken+"=======rrrrrrrrrrrrrrauthenticationTokenauthenticationToken");

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        System.out.println(auth+"111111111111authhhhhhHiiiiiiiiiiiiiiiiiiiiii");
        if (auth != null) {
            refreshTokenService.deleteRefreshToken(refreshtoken,authenticationToken);
            new SecurityContextLogoutHandler().logout(request, response, auth);

        }else{
            refreshTokenService.deleteRefreshToken(refreshtoken,authenticationToken);
        }
        System.out.println(auth+"authhhhhhHiiiiiiiiiiiiiiiiiiiiii");
    }
}